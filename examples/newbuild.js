project.defaults = 'jarsrc';
out.println(env["user.dir"]);

jarsrcname = 'jarsrc.jar'

cpath = new ant.fileset( {
	dir : 'src/main/java',
	includes : '**/*java'
});

function get_date() {
	return new java.util.Date();
}

function get_jarsrcname() {
	return io.File.createTempFile("tmp","jar");
}
target.init = function() {
	ant.copy( {
		todir : 'dupa',
		fileset : cpath
	});
}

target.postinit.depends = 'init';
target.postinit = function() {
	ant.echo( {
		message : "execution time" + get_date()
	});
}

target.jarsrc.depends = Array('postinit', 'init');
target.jarsrc.description = "creates jar source file"
target.jarsrc = function() {
	var tempfile = get_jarsrcname();
	out.println(tempfile);
	var fname = tempfile.getAbsolutePath();
	ant.jar( {
		destfile : fname,
		fileset : cpath
	});

	out.println("checkiffile");

	// in.readLine();

	var f = new io.File(fname);
	if (f.exists()) {
		out.println("file exsists "+fname);
	}
}
