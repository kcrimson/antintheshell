const ant = require("ant");
const java = require("java");
const ivy = require("ivy");

var javacp = ivy.dependencies(
    ["org.apache.commons:logging:1.4",
     "org.hibernate:hibernate-core:3.5"
    ]
);

buildSets.javasource = new BuildSet({
		nature : new java.Nature()
});

buildSets.javatests = new BuildSet({
		depends : 'javasource',
		natures : new java.TestNG()
});

targets.jar.depends = 'javasource';
targets.jar = function(){
	ant.Jar()
}

const javasources = new BuildSet(new java.Builder());

javasources.compile