const java = require("java");
const ant = require("ant");
const ivy = require("ivy");

var mainpath = ivy.dependencies({
	[
	 "org.apache.commons:common-lang:2.4"
	 ]
});

// defines java project builder
builders.main = new java.Builder();

// defines JUnit tests builder
builders.test = new java.JUnitBuilder(builders.main);

builders.webapp = new java.WebAppBuilder(builders.main,{
	webxml : "src/main/web.xml",
	libs : "";
});

targets.done = function(){
	ant.echo({message : "done"});
}
targets.done.description = "Outputs done";
targets.done.depends = [builders.main.compile,builders.test.run];