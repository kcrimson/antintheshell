//imports Java project builder/buildset
const java = imports.net.antshell.java;
const javaweb = imports.net.antshell.javaweb;
//define project name
project.name = "samples";

//define builder with default sources, output dir and jar file
const javasources = new java.BuildSet();

//this will make following targets available for you,
// javasources.compile
// javasources.clean
// javasources.jar
// javasources.jar-sources

//and following variables
// javasources.src

const tests = new junit.BuildSet();

//this will make following targets available for you,
// tests.compile
// tests.clean
// tests.run
// tests.report

targets.clean.depends = sources.clean, tests.clean
targets.clean = function(){
	
}

targets.build.depends = [sources.compile,tests.run,sources.jar];