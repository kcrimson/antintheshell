
const ant = imports.net.antshell.ant;


buildSets.java = BuildSet(
		buildPath : 'src/java/main'
		buildPath : { folder : 'src/java/main' , outputFolder : 'target/main/java' , excluded : '' , included : ''}
		natures : [
		           JavaNature(
		        		   classpath : ivy.resolveclasspath("compile")
		        		   jar : "target/jars/project.jar"
		        		   jar : {}
		           ),
		           JavaWebNature(
		        		   webxml : generateProdWebXml();
		           )
		           ],
		
);

buildSet.tests = BuildSet(
		depends : ['java']
)

project.nature = JavaNature(
		srcdir : 'src/main/java',
		targetdir : 'target/main/java'
		source : '1.5'
		target : '1.5'
)


project.nature = new JavaWebNature(
		webinf : 'src/java/web/WEB-INF'
)

target.runpmd=function(){
	
}

function generateProbWebXml(){
	
}