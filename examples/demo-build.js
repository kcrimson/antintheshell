project.defaults = 'jar';

srcdir = 'src/main/java';
srcpath = new ant.fileset( {
	dir : srcdir,
	includes : '**/*java'
});

builddir = 'build/main';

target.init = function() {
	ant.mkdir({dir:builddir});
}

target.compile.depends = 'init';
target.compile = function(){
	ant.javac({srcdir:srcdir,destdir:builddir});
	
	
};

target.jar.depends = 'compile';
target.jar.description = "creates jar source file";
target.jar = function() {
	var fname = 'example.jar';
	ant.jar( {
		destfile : fname,
		fileset : new ant.fileset({dir:builddir})
	});

	var f = new io.File(fname);
	if (f.exists()) {
		out.println("jar file created!!!");
	}
};