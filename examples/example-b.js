const java = imports.net.antshell.java;
const junit = imports.net.antshell.junit;
const ivy = imports.net.antshell.ivy;

project.name = "samples";

//define classpaths
const projectclasspath = new ivy.Dependencies([
"commons-collections:commons-collections:3.2",
"commons-lang:commons-lang:2.4"
]);

const testsclasspath = new ivy.Dependencies([
projectclasspath,
"junit:junit:4.7"
]);

//defines BuildSets
const javasources = new java.BuildSet({classpath : projectclasspath});
// or
// javasources.classpath = ivy.Dependencies({})
const javatests = new junit.BuildSet();

javasources.jar.depends = javatests.run;
javatests.compile.depends = javasources.compile;





// nature contributes targets and variables to project
// Java nature contributes clean,compile and jar
// javasources.classpath
// nature can ask other to contribute to its variables

//target.release.depends = javasources.jar;
//target.release = function(){
//	var f = javasources.jar.jarfile;
//	if(jarfile.exists()){
//		ant.copy({});
//	} else{
//	}		
//}