/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.model;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Collection;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author kcrimson@antshell.net
 * 
 */
public class ProjectTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void should_have_targets() throws Exception {
        final Project project = new Project();

        final Target target = new Target("buildstep01");
        project.addTarget(target);

        final Collection<Target> targets = project.getTargets();

        assertThat(targets).containsOnly(target);
        Target firstTarget = targets.iterator().next();
        assertThat(firstTarget.getProject()).isSameAs(project);
    }

    @Test
    public void should_have_unique_targets() throws Exception {
        final Project project = new Project();

        exception.expect(ProjectException.class);

        final Target target = new Target("buildstep01");
        project.addTarget(target);
        project.addTarget(target);

    }

    @Test
    public void should_execute_dependent_targets() throws Exception {
        final Project project = new Project();

        final Target target0 = new Target("step0");
        project.addTarget(target0);
        final Target target1 = new Target("step1");
        target1.addDepends("step0");
        project.addTarget(target1);
        final Target target2 = new Target("step2");
        project.addTarget(target2);

        final Executor executor = project.getExecutor("step1");

        final String[] targetsNames = executor.getTargets();
        assertThat(targetsNames).isEqualTo(new String[] { "step0", "step1" });
    }

}
