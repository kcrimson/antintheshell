/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.model;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author jpalka
 * 
 */
public class BuilderTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void should_have_ref_to_project() throws Exception {

        // given
        Project project = new Project();

        // when
        String builderName = "main";
        Builder builder = new Builder(builderName);
        project.addBuilder(builder);

        // then
        assertThat(builder.getProject()).isSameAs(project);
        assertThat(builder.hasPreviousBuilder()).isFalse();
    }

    @Test
    public void should_exception_when_name_null() throws Exception {
        // given
        Project project = new Project();
        String builderName = null;
        exception.expect(BuilderException.class);
        exception.expectMessage("builder name should not be null");
        // when
        Builder builder = new Builder(builderName);
    }

    @Test
    public void should_chain_builders() throws Exception {
        // given
        Project project = new Project();
        Builder firstBuilder = new Builder("main");
        project.addBuilder(firstBuilder);
        // when
        Builder nextBuilder = new Builder("test", firstBuilder);
        project.addBuilder(nextBuilder);

        // then
        assertThat(nextBuilder.getProject()).isSameAs(project);
        assertThat(nextBuilder.getPreviousBuilder()).isSameAs(firstBuilder);
        assertThat(nextBuilder.hasPreviousBuilder()).isTrue();
    }

    @Test
    public void should_contribute_targets_to_project() throws Exception {
        // given
        Project project = new Project();

        // when
        String builderName = "main";
        final String targetName = "compile";
        Builder builder = new Builder(builderName) {

            @Override
            public Map<String, Target> getTargets() {
                Map<String, Target> targets = new HashMap<String, Target>();
                targets.put(targetName, new Target(targetName));
                return targets;
            }
        };
        project.addBuilder(builder);

        // then
        assertThat(project.getTargets()).isNotEmpty();
        assertThat(project.getTarget("main.compile")).isNotNull();
        assertThat(project.getTarget("main.compile").getProject()).isSameAs(
                project);

    }

    @Test
    public void should_not_allow_duplicate_builders() throws Exception {
        // given
        Project project = new Project();

        Builder firstBuilder = new Builder("main");
        project.addBuilder(firstBuilder);
        exception.expect(BuilderException.class);
        exception.expectMessage("duplicate builder name");
        // when
        Builder secondBuilder = new Builder("main");
        project.addBuilder(secondBuilder);
    }

}
