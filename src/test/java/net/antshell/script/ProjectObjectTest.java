/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.script;

import static org.junit.Assert.assertEquals;
import net.antshell.packages.ant.ProjectDefinition;
import net.antshell.script.ScriptRuntime;
import net.antshell.script.ScriptRuntimeFactory;

import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

public class ProjectObjectTest {

    private ScriptRuntime runtime;
    private Scriptable projectscope;

    @Before
    public void setupRuntime() {
        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();

        runtime = fcty.createRuntime();

        projectscope = (Scriptable) ScriptableObject.getProperty(
                runtime.getScope(), ScriptRuntime.PROJECT_PROPERTY);
    }

    @Test
    public void set_default_target() {
        Context context = Context.getCurrentContext();

        context.evaluateString(runtime.getScope(),
                "project.defaults='compile'", "no name", 0, null);
        assertEquals("compile", runtime.getProject().getDefaultTargets());
        assertEquals("compile",
                ScriptableObject.getProperty(projectscope, "defaults"));
    }

}
