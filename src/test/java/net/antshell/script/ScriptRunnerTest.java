/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.script;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.FileNotFoundException;

import net.antshell.script.ScriptRunner;
import net.antshell.script.ScriptRuntime;
import net.antshell.script.ScriptRuntimeFactory;

import org.junit.Test;
import org.mozilla.javascript.ScriptableObject;

public class ScriptRunnerTest {

    @Test
    public void run_existing_script() throws Exception {

        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();

        ScriptRuntime runtime = fcty.createRuntime();

        ScriptRunner runner = new ScriptRunner(runtime, "demo-build.js", "");

        String[] targets = runner.getTargets();
        assertNull(targets);
    }

    @Test
    public void parse_build_arguments() throws Exception {
        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();
        ScriptRuntime runtime = fcty.createRuntime();

        ScriptRunner runner = new ScriptRunner(runtime, "filename.js",
                "jar build_version=1.1,build_profile=web");

        assertEquals(new String[] { "jar" }, runner.getTargets());
        // checking if parameters where parsed
        assertEquals("1.1", runner.getParameter("build_version"));
        assertEquals("web", runner.getParameter("build_profile"));
        // checking if parameters where set in script scope
        assertEquals("1.1", ScriptableObject.getProperty(runtime.getScope(),
                "build_version"));
        assertEquals("web", ScriptableObject.getProperty(runtime.getScope(),
                "build_profile"));

    }

    @Test
    public void parse_build_targets() throws Exception {

        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();
        ScriptRuntime runtime = fcty.createRuntime();

        ScriptRunner runner = new ScriptRunner(runtime, "filename.js", "jar");

        assertEquals(new String[] { "jar" }, runner.getTargets());

    }

    @Test
    public void parse_build_parameters() throws Exception {

        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();
        ScriptRuntime runtime = fcty.createRuntime();

        ScriptRunner runner = new ScriptRunner(runtime, "filename.js",
                "build_version=1.1");

        assertEquals("1.1", runner.getParameter("build_version"));

    }

    /**
     * fix for http://trac.assembla.com/antintheshell/ticket/6
     * 
     * @throws Exception
     */
    @Test(expected = FileNotFoundException.class)
    public void validate_build_file() throws Exception {
        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();
        ScriptRuntime runtime = fcty.createRuntime();

        ScriptRunner runner = new ScriptRunner(runtime, "filename.js", "");

        runner.exec();
    }
}
