/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.script;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.mozilla.javascript.ScriptableObject;
import net.antshell.model.*;

public class ScriptRuntimeFactoryTests {

    @Test
    public void create_default_script_runtime() {
        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();

        ScriptRuntime runtime = fcty.createRuntime();
        
        Project project = runtime.getProject();
        assertThat(project).isNotNull();
        
        Object targetsObject = ScriptableObject.getProperty(runtime.getScope(),
                "targets");
        assertThat(targetsObject).isInstanceOf(TargetsObject.class);

        Object projectObject = ScriptableObject.getProperty(runtime.getScope(),
                "project");
        assertThat(projectObject).isInstanceOf(ProjectObject.class);

        Object buildersObject = ScriptableObject.getProperty(
                runtime.getScope(), "builders");
        assertThat(buildersObject).isInstanceOf(BuildersObject.class);

    }

}
