/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.PatternSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.EvaluatorException;
import org.mozilla.javascript.JavaScriptException;
import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

public class ProjectComponentDefinitionTest {

    private Context context;
    private Scriptable scope;
    private Project project;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        context = ContextFactory.getGlobal().enterContext();
        scope = context.initStandardObjects();

        // init ant object
        NativeObject antObject = new NativeObject();
        scope.put("ant", scope, antObject);
        PackageExports.exportPackage(antObject);

        project = new Project();
        project.init();
    }

    @Test
    public void should_construct_ant_type() {
        context
                .evaluateString(scope, "var f = new ant.fileset();", "", 0,
                        null);
        Object object = scope.get("f", scope);
        assertThat(object).isNotNull().isInstanceOf(
                ProjectComponentAdapter.class);
    }

    @Test
    public void should_not_call_ant_type() {
        expectedException.expect(EvaluatorException.class);
        expectedException.expectMessage("Ant types are not callable");
        context.evaluateString(scope, "ant.fileset();", "", 0, null);
    }

    @Test
    public void should_construct_ant_task() {
        context.evaluateString(scope, "var f = new ant.echo();", "", 0, null);
        Object object = scope.get("f", scope);
        assertThat(object).isNotNull().isInstanceOf(TaskAdapter.class);
    }

    @Test
    public void should_call_constructed_ant_task() {
        context.evaluateString(scope, "var f = new ant.echo();f();", "", 0,
                null);
        Object object = scope.get("f", scope);
        assertThat(object).isNotNull().isInstanceOf(TaskAdapter.class);
    }

    @Test
    public void should_call_ant_task() {
        Object evaluateObj = context.evaluateString(scope, "ant.echo();", "",
                0, null);
        assertThat(evaluateObj).isNotNull().isSameAs(
                Context.getUndefinedValue());
    }

    @Test
    public void create_new_type_instance() {

        ProjectComponentDefinition def = new ProjectComponentDefinition(
                project, "fileset");

        ProjectComponentAdapter proxy = (ProjectComponentAdapter) def
                .construct(context, scope, new Object[] {});

        assertNotNull(proxy);
    }

    @Test
    public void create_new_type_instance_args() {

        ProjectComponentDefinition def = new ProjectComponentDefinition(
                project, "fileset");

        NativeObject arg = new NativeObject();
        arg.defineProperty("dir", "src/java", ScriptableObject.EMPTY);

        ProjectComponentAdapter proxy = (ProjectComponentAdapter) def
                .construct(context, scope, new Object[] { arg });

        String attr = proxy.getAttribute("dir");
        assertEquals("src/java", attr);

    }

    @Test
    public void create_new_type_set_args() {
        ProjectComponentDefinition def = new ProjectComponentDefinition(
                project, "fileset");
        ProjectComponentAdapter proxy = (ProjectComponentAdapter) def
                .construct(context, scope, new Object[] {});
        ScriptableObject.defineProperty(proxy, "dir", "src/java",
                ScriptableObject.EMPTY);

        String attr = proxy.getAttribute("dir");
        assertEquals("src/java", attr);

    }

    @Test
    public void create_new_type_overide_args() {
        ProjectComponentDefinition def = new ProjectComponentDefinition(
                project, "fileset");
        ProjectComponentAdapter proxy = (ProjectComponentAdapter) def
                .construct(context, scope, new Object[] {});

        ScriptableObject.defineProperty(proxy, "includesfile", "src/java",
                ScriptableObject.EMPTY);

        ScriptableObject.defineProperty(proxy, "includesfile",
                new ProjectComponentAdapter(project,
                        PatternSet.NameEntry.class, null),
                ScriptableObject.EMPTY);

        String attr = proxy.getAttribute("includesfile");
        assertNull(attr);

        Scriptable scriptable = proxy.getNestedElement("includesfile");
        assertNotNull(scriptable);

    }

    @Test
    public void create_new_type_invert_overide_args() {
        ProjectComponentDefinition def = new ProjectComponentDefinition(
                project, "fileset");
        ProjectComponentAdapter proxy = (ProjectComponentAdapter) def
                .construct(context, scope, new Object[] {});

        ScriptableObject.defineProperty(proxy, "includesfile",
                new ProjectComponentAdapter(project,
                        PatternSet.NameEntry.class, null),
                ScriptableObject.EMPTY);

        ScriptableObject.defineProperty(proxy, "includesfile", "src/java",
                ScriptableObject.EMPTY);

        Scriptable scriptable = proxy.getNestedElement("includesfile");
        assertNull(scriptable);

        String attr = proxy.getAttribute("includesfile");
        assertEquals("src/java", attr);

    }

}
