/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import net.antshell.packages.ant.ProjectComponentAdapter;

import org.apache.tools.ant.Project;
import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.ScriptableObject;

public class ProjectComponentAdapterTest {

    private Project project;

    @Before
    public void setup() {
        project = new Project();
        project.init();
    }

    @Test
    public void storeAttribute() {

        ProjectComponentAdapter proxy = new ProjectComponentAdapter(project,
                "javac", null);

        ScriptableObject.defineProperty(proxy, "srcdir", "src/java/main",
                ScriptableObject.EMPTY);

        assertEquals("src/java/main", proxy.getAttribute("srcdir"));

    }

    @Test
    public void storeInvalidAttribute() {

        ProjectComponentAdapter proxy = new ProjectComponentAdapter(project,
                "javac", null);

        assertNull(proxy.getAttribute("someattr"));
    }

    @Test
    public void storeNestedElement() {

        ProjectComponentAdapter proxy = new ProjectComponentAdapter(project,
                "javac", null);

        ScriptableObject.defineProperty(proxy, "src",
                new ProjectComponentAdapter(project, "fileset", null),
                ScriptableObject.EMPTY);

        assertNotNull(proxy.getNestedElement("src"));

    }
}
