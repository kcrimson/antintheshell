/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import net.antshell.script.ScriptRuntime;
import net.antshell.script.ScriptRuntimeFactory;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class TargetDefinitionTest {

    private ScriptRuntime runtime;
    private Context context;
    private Scriptable scope;
    private TargetDefinition definition;

    @Before
    public void setup() {
        BasicConfigurator.configure();

        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();

        runtime = fcty.createRuntime();

        context = Context.getCurrentContext();
        scope = runtime.getScope();

    }

    @Test
    public void define_callable_target() {

        context.evaluateString(scope, "target.init = function(){};", "no name",
                0, null);

        Scriptable target = definition.getTarget("init");
        assertNotNull(target);
    }

    @Test
    public void define_target_depends() {

        context.evaluateString(scope, "target.init.depends = 'clean';",
                "no name", 0, null);

        Scriptable target = definition.getTarget("init");
        assertNotNull(target);
    }

    @Test
    public void define_callable_target_depends() {

        context.evaluateString(scope,
                "target.init.depends = 'clean';target.init = function(){};",
                "no name", 0, null);

        TargetAdapter target = (TargetAdapter) definition.getTarget("init");
        assertEquals("clean", target.getDependencies().nextElement());
        assertTrue(target.isCallable());
    }
}
