/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Enumeration;

import net.antshell.packages.ant.TargetAdapter;
import net.antshell.packages.ant.TargetDefinition;
import net.antshell.script.ScriptRuntime;
import net.antshell.script.ScriptRuntimeFactory;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

public class TargetAdapterTest {

    private ScriptRuntime runtime;
    private Context context;
    private Scriptable scope;
    private TargetDefinition definition;

    @Before
    public void setup() {
        BasicConfigurator.configure();

        ScriptRuntimeFactory runtimeFactory = new ScriptRuntimeFactory();
        runtime = runtimeFactory.createRuntime();

        context = Context.getCurrentContext();
        scope = runtime.getScope();

    }

    @Test
    public void run_target() {

        context.evaluateString(
                scope,
                "targets.init = function(){targetDone = 'true';}; targets.init();",
                "no name", 0, null);

        assertEquals("true", ScriptableObject.getProperty(scope, "targetDone"));

        TargetAdapter adapter = (TargetAdapter) ScriptableObject.getProperty(
                definition, "init");
        assertFalse(adapter.getDependencies().hasMoreElements());
    }

    @Test
    public void define_single_depends() {

        context.evaluateString(
                scope,
                "targets.init0 = function(){}; targets.init1.depends='init0';targets.init1 = function(){};",
                "no name", 0, null);

        TargetAdapter adapter = (TargetAdapter) ScriptableObject.getProperty(
                definition, "init1");
        assertEquals("init0", adapter.getDependencies().nextElement());

    }

    @Test
    public void define_multiple_depends() {

        context.evaluateString(
                scope,
                "targets.init0 = function(){}; targets.init1 = function(){}; targets.init2.depends=Array('init0','init1');targets.init2 = function(){};",
                "no name", 0, null);

        TargetAdapter adapter = (TargetAdapter) ScriptableObject.getProperty(
                definition, "init2");
        Enumeration<String> dependencies = adapter.getDependencies();
        assertEquals("init0", dependencies.nextElement());
        assertEquals("init1", dependencies.nextElement());
    }

    @Test
    public void execute_dependent_tasks() {

        String script = "targets.init0 = function(){init0executed='true'};"
                + "targets.init1 = function(){init1executed='true'}; "
                + "targets.init2.depends='init0';target.init2=function(){init2executed='true'};"
                + "targets.init3.depends=Array('init1','init2');"
                + "targets.init3 = function(){init3executed='true'};targets.init3();";

        context.evaluateString(scope, script, "no name", 0, null);

        assertEquals("true",
                ScriptableObject.getProperty(scope, "init0executed"));
        assertEquals("true",
                ScriptableObject.getProperty(scope, "init1executed"));
        assertEquals("true",
                ScriptableObject.getProperty(scope, "init2executed"));
        assertEquals("true",
                ScriptableObject.getProperty(scope, "init3executed"));

    }
}
