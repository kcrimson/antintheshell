/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.script;

import static org.mozilla.javascript.ScriptableObject.PERMANENT;
import static org.mozilla.javascript.ScriptableObject.READONLY;
import static org.mozilla.javascript.ScriptableObject.defineProperty;

import net.antshell.model.Project;

import org.mozilla.javascript.ScriptableObject;

public class ScriptRuntime {

    public static final String PROJECT_PROPERTY = "project";
    private final ScriptableObject fScope;
    private final TargetsObject targets;
    private final ProjectObject projectObject;
    private final BuildersObject builders;
    private final Project fProject;

    public ScriptRuntime(ScriptableObject scope) {
        fScope = scope;

        fProject = new Project();

        projectObject = new ProjectObject(fProject);
        defineProperty(fScope, PROJECT_PROPERTY, projectObject, PERMANENT
                | READONLY);

        targets = new TargetsObject();
        defineProperty(fScope, "targets", targets, PERMANENT | READONLY);

        builders = new BuildersObject();
        defineProperty(fScope, "builders", builders, PERMANENT | READONLY);

    }

    public ScriptableObject getScope() {
        return fScope;
    }

    /**
     * @param strings
     */
    public void executeTargets(String[] strings) throws ShellException {
        // TODO Auto-generated method stub

    }

    /**
     * @return
     */
    public Project getProject() {
        return fProject;
    }

}
