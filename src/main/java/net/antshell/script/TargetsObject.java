/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.script;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

/**
 * @author jpalka
 * 
 */
public class TargetsObject extends ScriptableObject {

    /**
     * 
     */
    private static final long serialVersionUID = 4962384270508083518L;

    /*
     * (non-Javadoc)
     * 
     * @see org.mozilla.javascript.ScriptableObject#put(java.lang.String,
     * org.mozilla.javascript.Scriptable, java.lang.Object)
     */
    @Override
    public void put(String name, Scriptable start, Object value) {
        if (!(value instanceof Function)) {
            throw Context.reportRuntimeError("target needs to be a function");
        }
        super.put(name, start, value);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.mozilla.javascript.ScriptableObject#put(int,
     * org.mozilla.javascript.Scriptable, java.lang.Object)
     */
    @Override
    public void put(int index, Scriptable start, Object value) {
        // TODO Auto-generated method stub
        super.put(index, start, value);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.mozilla.javascript.ScriptableObject#getClassName()
     */
    @Override
    public String getClassName() {
        return "targets";
    }

}
