/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.script;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.antshell.launcher.Launcher;

public class DefaultLauncher implements Launcher {

    @Override
    public void launch(File buildfile, String shellargs)
            throws FileNotFoundException, IOException {
        ScriptRuntimeFactory fcty = new ScriptRuntimeFactory();

        ScriptRuntime runtime = fcty.createRuntime();

        ScriptRunner runner = new ScriptRunner(runtime, buildfile, shellargs);

        runner.exec();

    }

}
