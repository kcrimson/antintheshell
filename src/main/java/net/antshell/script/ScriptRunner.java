/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.script;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.antshell.packages.ant.TargetAdapter;
import net.antshell.packages.ant.TargetDefinition;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ScriptableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptRunner {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ScriptRunner.class);

    private static final Pattern PARAMETER_PATTERN = Pattern
            .compile("([\\D&&[^,]]\\w*)=(([\\p{Graph}&&[^=,]])*)");

    private static final Pattern ARGUMENTS_PATTERNS = Pattern
            .compile("([\\w,]*)\\s+(\\p{Graph}*)");

    private final ScriptRuntime fRuntime;

    private String[] fTargets;

    private Properties fParameters;

    private final File fBuildfile;

    public ScriptRunner(ScriptRuntime runtime, String filename, String shellargs)
            throws IOException {
        fRuntime = runtime;

        fBuildfile = new File(filename);
        parseArguments(shellargs);
    }

    /**
     * @param runtime
     * @param buildfile
     * @param shellargs
     */
    public ScriptRunner(ScriptRuntime runtime, File buildfile, String shellargs) {
        fRuntime = runtime;

        fBuildfile = buildfile;
        parseArguments(shellargs);
    }

    public void exec() throws FileNotFoundException, IOException {

        LOGGER.debug("running script {}", fBuildfile);

        Context context = Context.getCurrentContext();

        context.evaluateReader(fRuntime.getScope(), new FileReader(fBuildfile),
                null, 0, null);

        TargetDefinition def = (TargetDefinition) ScriptableObject.getProperty(
                fRuntime.getScope(), "target");

        if (fTargets == null || fTargets.length <= 0) {
            String defaultTarget = fRuntime.getProject().getDefaultTargets();
            if (defaultTarget != null) {

                LOGGER.debug("setting default target:  {}", defaultTarget);

                fTargets = new String[] { defaultTarget };
            }
        }

        if (fTargets != null && fTargets.length > 0) {
            List<TargetAdapter> adapters = def.sortDependencies(fTargets);

            if (adapters != null && adapters.size() > 0) {
                LOGGER.debug("executing targets: {}", adapters);
            }

            for (TargetAdapter adapter : adapters) {
                adapter.getCallable().call(context,
                        fRuntime.getScope(), def, new Object[] {});
            }
        }

    }

    public String[] getTargets() {
        return fTargets;
    }

    public String getParameter(String key) {
        if (fParameters != null) {
            return fParameters.getProperty(key);
        }
        return null;
    }

    private void parseArguments(String arguments) {

        LOGGER.debug("parsing command line arguments: \"{}\"", arguments);

        Matcher argumentMatcher = ARGUMENTS_PATTERNS.matcher(arguments);
        if (argumentMatcher.matches()) {
            String targets = argumentMatcher.group(1);
            String parameters = argumentMatcher.group(2);

            fTargets = parseTargets(targets);

            fParameters = parseParameters(parameters);
        } else {
            fParameters = parseParameters(arguments);
            if (fParameters == null) {
                fTargets = parseTargets(arguments);
            }
        }

        if (fParameters != null && fParameters.size() > 0) {
            for (Map.Entry<Object, Object> entry : fParameters.entrySet()) {
                ScriptableObject.defineProperty(fRuntime.getScope(),
                        (String) entry.getKey(), entry.getValue(),
                        ScriptableObject.PERMANENT | ScriptableObject.READONLY);
            }
        }
    }

    private Properties parseParameters(String parameters) {
        Properties properties = new Properties();
        Matcher matcher = PARAMETER_PATTERN.matcher(parameters);
        while (matcher.find()) {
            String key = matcher.group(1);
            String value = matcher.group(2);
            properties.put(key.trim(), value.trim());
        }

        properties = properties.size() > 0 ? properties : null;

        if (properties != null) {
            LOGGER.debug("parsed command line parameters: {}", properties);
        }

        return properties;
    }

    private String[] parseTargets(String targets) {

        if (targets != null && !targets.trim().isEmpty()) {
            String[] strings = targets.split(",");

            LOGGER.debug("parsed command line targets: \"{}\"", strings);

            return strings;
        }
        return null;
    }

}
