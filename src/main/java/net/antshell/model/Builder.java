/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.model;

import java.util.Map;

/**
 * @author jpalka
 * 
 */
public class Builder {

    private Project project;
    private Builder previousBuilder;
    private final String name;

    /**
     * @param project
     * @param name
     * @throws BuilderException
     */
    public Builder(final String name) throws BuilderException {

        if (name == null) {
            throw new BuilderException("builder name should not be null");
        }

        this.name = name;

    }

    /**
     * @param project
     * @param previousBuilder
     * @throws BuilderException
     */
    public Builder(String name, Builder previousBuilder)
            throws BuilderException {
        this(name);
        this.previousBuilder = previousBuilder;
    }

    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }

    /**
     * @return the previousBuilder
     */
    public Builder getPreviousBuilder() {
        return previousBuilder;
    }

    /**
     * @return
     */
    public boolean hasPreviousBuilder() {
        return previousBuilder != null;
    }

    /**
     * Override this method to contribute targets by this builder to project
     * TODO come with more flexible idea of defining available targets in this
     * builder
     * 
     * @return collection of builder targets
     * 
     */
    public Map<String, Target> getTargets() {
        return null;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param project2
     */
    public void setProject(Project project) {
        this.project = project;
    }

}
