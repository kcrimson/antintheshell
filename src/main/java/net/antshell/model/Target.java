/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jpalka
 * 
 */
public class Target {

    private final String name;
    private Project project;
    private List<String> dependencies = new ArrayList<String>();

    /**
     * @param string
     */
    public Target(String name) {
        this.name = name;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public String[] getDepends() {
        return dependencies.toArray(new String[] {});
    }

    /**
     * @param string
     */
    public void addDepends(String string) {
        dependencies.add(string);
    }

    /**
     * @return
     */
    public Project getProject() {
        return project;
    }

    /**
     * @param project
     */
    public void setProject(Project project) {
        this.project = project;
    }

}
