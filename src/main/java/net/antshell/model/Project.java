/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author jpalka
 * 
 */
public class Project {

    private final Map<String, Target> targets = new HashMap<String, Target>();
    private final Map<String, Builder> builders = new HashMap<String, Builder>();
    private String defaultTargets;

    /**
     * @param name
     * @param target
     * @throws ProjectException
     */
    public void addTarget(final Target target) throws ProjectException {
        final String name = target.getName();
        addTarget(name, target);
    }

    /**
     * @param string
     * @return
     * @throws ProjectException
     */
    public Executor getExecutor(final String targetName)
            throws ProjectException {
        // create list of steps to sort,
        final Set<String> targetsToSort = getTargetsToSort(targetName);

        // wrap build steps into SortedBuildStep
        final Map<String, SortedTarget> wrappedTargets = wrapTargetsToSort(targetsToSort);
        final List<String> sortedTargets = sortTargets(wrappedTargets);

        // create BuildStep executor
        return createExecutor(sortedTargets);
    }

    private Executor createExecutor(final List<String> sortedTargets) {
        return new Executor() {

            @Override
            public String[] getTargets() {
                return sortedTargets.toArray(new String[] {});
            }
        };
    }

    private List<String> sortTargets(
            final Map<String, SortedTarget> wrappedBuildSteps) {
        final List<String> sortedSteps = new ArrayList<String>();
        // do topological sort
        for (final SortedTarget step : wrappedBuildSteps.values()) {
            step.visit(wrappedBuildSteps, sortedSteps);
        }
        return sortedSteps;
    }

    private Set<String> getTargetsToSort(final String buildStepName)
            throws ProjectException {

        final Target target = targets.get(buildStepName);
        if (target == null) {
            throw new ProjectException();
        }

        final Set<String> stepsToSort = new HashSet<String>();
        stepsToSort.add(buildStepName);

        for (final String step : target.getDepends()) {
            stepsToSort.addAll(getTargetsToSort(step));
        }

        return stepsToSort;
    }

    private Map<String, SortedTarget> wrapTargetsToSort(
            final Set<String> targetsToSort) {
        final Map<String, SortedTarget> wrappedTargets = new LinkedHashMap<String, SortedTarget>();
        Target target;
        for (final String targetsName : targetsToSort) {
            target = targets.get(targetsName);
            wrappedTargets.put(targetsName, new SortedTarget(targetsName,
                    target));
        }
        return wrappedTargets;
    }

    /**
     * @return
     */
    public Collection<Target> getTargets() {
        return targets.values();
    }

    /**
     * @param targetName
     * @return
     */
    public Target getTarget(String targetName) {
        return targets.get(targetName);
    }

    /**
     * @param builder
     * @throws BuilderException
     * @throws ProjectException
     */
    public void addBuilder(Builder builder) throws BuilderException,
            ProjectException {

        if (builders.containsKey(builder.getName())) {
            throw new BuilderException("duplicate builder name");
        }

        builder.setProject(this);
        builders.put(builder.getName(), builder);

        Map<String, Target> builderTargets = builder.getTargets();
        if (builderTargets != null) {
            for (Map.Entry<String, Target> entry : builderTargets.entrySet()) {
                Target target = entry.getValue();
                String targetName = getTargetName(builder, target);
                addTarget(targetName, target);
            }
        }
    }

    /**
     * @param targetName
     * @param target
     * @throws ProjectException
     */
    public void addTarget(String targetName, Target target)
            throws ProjectException {
        if (targets.containsKey(targetName)) {
            throw new ProjectException();
        }
        target.setProject(this);
        targets.put(targetName, target);

    }

    private String getTargetName(Builder builder, Target target) {
        return builder.getName() + "." + target.getName();
    }

    /**
     * Helper class used during topological sort
     * 
     */
    private final class SortedTarget {

        private final String name;
        private final Target target;
        private boolean visited;

        public SortedTarget(final String name, final Target target) {
            super();
            this.name = name;
            this.target = target;
        }

        public boolean isVisited() {
            return visited;
        }

        public void visit(final Map<String, SortedTarget> wrappedTargets,
                final List<String> sortedTargets) {
            if (!isVisited()) {
                visited = true;
                final String[] dependencies = target.getDepends();
                for (final String targetName : dependencies) {
                    wrappedTargets.get(targetName).visit(wrappedTargets,
                            sortedTargets);
                }
                sortedTargets.add(this.name);
                System.out.println(this.name);
            }
        }
    }

    /**
     * @return
     */
    public String getDefaultTargets() {
        return defaultTargets;
    }

    /**
     * @param jsToJava
     */
    public void setDefaultTargets(String defaultTargets) {
        this.defaultTargets = defaultTargets;
    }
}