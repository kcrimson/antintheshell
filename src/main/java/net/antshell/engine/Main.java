/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.engine;

import java.io.IOException;

import net.antshell.script.ScriptRunner;
import net.antshell.script.ScriptRuntime;
import net.antshell.script.ScriptRuntimeFactory;

/**
 * Implementation of Narwhal engine, based on Rhino with addtional objects
 * required by antshell
 * 
 * @author jpalka
 * 
 */
public class Main {

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        ScriptRuntimeFactory runtimeFactory = new ScriptRuntimeFactory();

        ScriptRuntime scriptRuntime = runtimeFactory.createRuntime();

        ScriptRunner runner = new ScriptRunner(scriptRuntime, "", "");
        runner.exec();

    }

}
