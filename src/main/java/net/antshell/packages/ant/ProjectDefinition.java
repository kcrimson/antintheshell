/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import net.antshell.script.ScriptRuntime;

import org.apache.tools.ant.Project;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

public class ProjectDefinition extends ScriptableObject {

    private static final String DEFAULTS_ID = "defaults";
    private static final String NAME_ID = "name";
    private static final String BASEDIR_ID = "basedir";

    private static final String[] IDS = new String[] { DEFAULTS_ID, NAME_ID,
            BASEDIR_ID };

    private final ScriptRuntime fRuntime;
    private String fName;
    private String fDefault;
    private String fBaseDir;

    public ProjectDefinition(ScriptRuntime runtime) {
        fRuntime = runtime;
    }

    public static void init(ScriptRuntime runtime) {
        ProjectDefinition def = new ProjectDefinition(runtime);

        ScriptableObject.defineProperty(runtime.getScope(),
                ScriptRuntime.PROJECT_PROPERTY, def, ScriptableObject.PERMANENT
                        | ScriptableObject.READONLY);

    }

    @Override
    public String getClassName() {
        return ScriptRuntime.PROJECT_PROPERTY;
    }

    @Override
    public void put(String name, Scriptable start, Object value) {
        if (NAME_ID.equals(name)) {
            String strValue = (String) Context.jsToJava(value, String.class);
            fName = strValue;
            getProject().setName(strValue);
            return;
        }

        if (DEFAULTS_ID.equals(name)) {
            String strValue = (String) Context.jsToJava(value, String.class);
            fDefault = strValue;
            getProject().setDefault(strValue);
            return;
        }

        if (BASEDIR_ID.equals(name)) {
            String strValue = (String) Context.jsToJava(value, String.class);
            fBaseDir = strValue;
            getProject().setBasedir(fBaseDir);
        }

        super.put(name, start, value);
    }

    @Override
    public Object get(String name, Scriptable start) {
        if (NAME_ID.equals(name)) {
            return fName;
        }
        if (DEFAULTS_ID.equals(name)) {
            return fDefault;
        }
        if (BASEDIR_ID.equals(name)) {
            return fBaseDir;
        }
        return super.get(name, start);
    }

    private Project getProject() {
        return null;//fRuntime.getProject();
    }

}
