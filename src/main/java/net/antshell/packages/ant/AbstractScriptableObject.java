/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import org.mozilla.javascript.Scriptable;

/**
 * 
 * @author kcrimson
 * 
 */
public abstract class AbstractScriptableObject implements Scriptable {

    private Scriptable fParentScope;
    private Scriptable fPrototype;

    @Override
    public void delete(String name) {
    }

    @Override
    public void delete(int idx) {
    }

    @Override
    public Object get(String name, Scriptable scope) {
        return NOT_FOUND;
    }

    @Override
    public Object get(int idx, Scriptable scope) {
        return NOT_FOUND;
    }

    @Override
    public String getClassName() {
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object getDefaultValue(Class clazz) {
        return null;
    }

    @Override
    public Object[] getIds() {
        return null;
    }

    @Override
    public Scriptable getParentScope() {
        return fParentScope;
    }

    @Override
    public Scriptable getPrototype() {
        return fPrototype;
    }

    @Override
    public boolean has(String name, Scriptable scope) {
        return false;
    }

    @Override
    public boolean has(int idx, Scriptable scope) {
        return false;
    }

    @Override
    public boolean hasInstance(Scriptable scriptable) {
        return false;
    }

    @Override
    public void put(String name, Scriptable scope, Object value) {

    }

    @Override
    public void put(int idx, Scriptable scope, Object value) {
    }

    @Override
    public void setParentScope(Scriptable scope) {
        fParentScope = scope;
    }

    @Override
    public void setPrototype(Scriptable prototype) {
        fPrototype = prototype;
    }

}
