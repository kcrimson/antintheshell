/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import org.apache.tools.ant.ComponentHelper;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectComponent;
import org.apache.tools.ant.Task;
import org.mozilla.javascript.Callable;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

/**
 * @author SG0205373
 * 
 */
public class TaskAdapter extends ProjectComponentAdapter implements Callable {

    private final String fComponentName;

    /**
     * @param project
     * @param componentName
     * @param arg
     */
    public TaskAdapter(Project project, String componentName, Scriptable arg) {
        super(project, componentName, arg);
        this.fComponentName = componentName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.mozilla.javascript.Callable#call(org.mozilla.javascript.Context,
     * org.mozilla.javascript.Scriptable, org.mozilla.javascript.Scriptable,
     * java.lang.Object[])
     */
    @Override
    public Object call(Context cx, Scriptable scope, Scriptable thisObj,
            Object[] args) {
        Task task = ComponentHelper.getComponentHelper(fProject).createTask(
                fComponentName);
        configure(task);
        task.setTaskName(fComponentName);
        try {
            task.execute();
        } catch (Exception e) {
            Context.throwAsScriptRuntimeEx(e);
        }
        return Context.getUndefinedValue();
    }

}
