/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

import org.apache.tools.ant.Project;
import org.mozilla.javascript.ScriptableObject;

/**
 * @author SG0205373
 * 
 */
public final class PackageExports {

    public static void exportPackage(ScriptableObject scriptable) {

        Project project = new Project();
        project.init();
        Hashtable<?, ?> tasks = project.getTaskDefinitions();

        ArrayList<?> tasksList = Collections.list(tasks.keys());

        String componentName;
        for (Object entry : tasksList) {
            componentName = (String) entry;
            scriptable.defineProperty(componentName,
                    new ProjectComponentDefinition(project, componentName),
                    ScriptableObject.PERMANENT | ScriptableObject.READONLY);
        }

        Hashtable<?, ?> types = project.getDataTypeDefinitions();
        ArrayList<?> typesList = Collections.list(types.keys());
        for (Object entry : typesList) {
            componentName = (String) entry;
            scriptable.defineProperty(componentName,
                    new ProjectComponentDefinition(project, componentName),
                    ScriptableObject.PERMANENT | ScriptableObject.READONLY);
        }
    }

}
