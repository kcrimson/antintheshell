/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import org.apache.tools.ant.ComponentHelper;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Undefined;

/**
 * 
 * @author kcrimson
 * 
 */
public class ProjectComponentDefinition extends AbstractScriptableObject
        implements Function {

    private final String fComponentName;
    private final Project fProject;

    public ProjectComponentDefinition(Project project, String componentName) {
        fProject = project;
        fComponentName = componentName;
    }

    @Override
    public Object call(Context context, Scriptable scope, Scriptable thisObj,
            Object[] args) {

        Scriptable arg = null;

        if (args != null && args.length == 1) {
            arg = (Scriptable) args[0];
        }

        ProjectComponentAdapter adapter = new ProjectComponentAdapter(fProject,
                fComponentName, arg);

        Class<?> componentClass = ComponentHelper.getComponentHelper(fProject)
                .getComponentClass(fComponentName);
        if (Task.class.isAssignableFrom(componentClass)) {
            final Task task = ComponentHelper.getComponentHelper(fProject)
                    .createTask(fComponentName);

            adapter.configure(task);
            task.setTaskName(fComponentName);

            try {
                task.execute();
            } catch (final Exception e) {
                Context.throwAsScriptRuntimeEx(e);
            }
            return Undefined.instance;
        }

        throw Context.reportRuntimeError("Ant types are not callable");
    }

    @Override
    public Scriptable construct(Context context, Scriptable scope, Object[] args) {

        Scriptable arg = null;

        if (args != null && args.length == 1) {
            arg = (Scriptable) args[0];
        }

        Class<?> componentClass = ComponentHelper.getComponentHelper(fProject)
                .getComponentClass(fComponentName);

        if (Task.class.isAssignableFrom(componentClass)) {
            return new TaskAdapter(fProject, fComponentName, arg);
        } else {
            return new ProjectComponentAdapter(fProject, fComponentName, arg);
        }

    }
}
