/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import net.antshell.script.ScriptRuntime;

import org.apache.tools.ant.BuildException;
import org.mozilla.javascript.Callable;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Definition of 'target' object behavior
 * 
 * This class uses implementation of topoSort from Ant.
 * 
 * @author kcrimson
 * 
 */
public class TargetDefinition extends AbstractScriptableObject {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(TargetDefinition.class);

    /**
     * Constant for the &quot;visiting&quot; state, used when traversing a DFS
     * of target dependencies.
     */
    private static final String VISITING = "VISITING";

    /**
     * Constant for the &quot;visited&quot; state, used when traversing a DFS of
     * target dependencies.
     */
    private static final String VISITED = "VISITED";

    private final Map<String, TargetAdapter> fTargets = new HashMap<String, TargetAdapter>();

    private final ScriptRuntime fRuntime;

    public TargetDefinition(ScriptRuntime runtime) {
        super();
        setParentScope(runtime.getScope());
        fRuntime = runtime;
    }

    public Scriptable getTarget(String name) {
        return fTargets.get(name);
    }

    @Override
    public Object get(String name, Scriptable scope) {
        TargetAdapter adapter = fTargets.get(name);

        if (adapter == null) {
            adapter = new TargetAdapter(fRuntime, name);
            fTargets.put(name, adapter);
        }

        return adapter;
    }

    @Override
    public Object[] getIds() {
        return fTargets.keySet().toArray(new String[] {});
    }

    @Override
    public boolean has(String name, Scriptable scope) {
        return fTargets.containsKey(name);
    }

    @Override
    public void put(String name, Scriptable scope, Object value) {

        TargetAdapter adapter = fTargets.get(name);

        if (adapter == null) {
            adapter = new TargetAdapter(fRuntime, name, (Callable) value);

            fTargets.put(name, adapter);
        } else {
            adapter.setCallable((Callable) value);
        }

    }

    // copied from Ant project/topological sort

    /**
     * Topologically sort a set of targets.
     * 
     * @param root
     *            <code>String[]</code> containing the names of the root
     *            targets. The sort is created in such a way that the ordered
     *            sequence of Targets is the minimum possible such sequence to
     *            the specified root targets. Must not be <code>null</code>.
     * @param targetTable
     *            A map of names to targets (String to Target). Must not be
     *            <code>null</code>.
     * @param returnAll
     *            <code>boolean</code> indicating whether to return all targets,
     *            or the execution sequence only.
     * @return a Vector of Target objects in sorted order.
     * @exception BuildException
     *                if there is a cyclic dependency among the targets, or if a
     *                named target does not exist.
     * @since Ant 1.6.3
     */
    public final List<TargetAdapter> sortDependencies(String[] root,
            Map<String, TargetAdapter> targetTable, boolean returnAll)
            throws BuildException {
        List<TargetAdapter> ret = new ArrayList<TargetAdapter>();
        Map<String, String> state = new HashMap<String, String>();
        Stack<String> visiting = new Stack<String>();

        // We first run a DFS based sort using each root as a starting node.
        // This creates the minimum sequence of Targets to the root node(s).
        // We then do a sort on any remaining unVISITED targets.
        // This is unnecessary for doing our build, but it catches
        // circular dependencies or missing Targets on the entire
        // dependency tree, not just on the Targets that depend on the
        // build Target.

        for (int i = 0; i < root.length; i++) {
            String st = (state.get(root[i]));
            if (st == null) {
                tsort(root[i], targetTable, state, visiting, ret);
            } else if (st == VISITING) {
                throw new RuntimeException(
                        "Unexpected node in visiting state: " + root[i]);
            }
        }
        StringBuffer buf = new StringBuffer("Build sequence for target(s)");

        for (int j = 0; j < root.length; j++) {
            buf.append((j == 0) ? " `" : ", `").append(root[j]).append('\'');
        }
        buf.append(" is " + ret);
        System.out.println(buf.toString());

        List<TargetAdapter> complete = (returnAll) ? ret
                : new ArrayList<TargetAdapter>(ret);
        for (Iterator<?> en = targetTable.keySet().iterator(); en.hasNext();) {
            String curTarget = (String) en.next();
            String st = state.get(curTarget);
            if (st == null) {
                tsort(curTarget, targetTable, state, visiting, complete);
            } else if (st == VISITING) {
                throw new RuntimeException(
                        "Unexpected node in visiting state: " + curTarget);
            }
        }
        System.out.println("Complete build sequence is " + complete);
        return ret;
    }

    /**
     * Perform a single step in a recursive depth-first-search traversal of the
     * target dependency tree.
     * <p>
     * The current target is first set to the &quot;visiting&quot; state, and
     * pushed onto the &quot;visiting&quot; stack.
     * <p>
     * An exception is then thrown if any child of the current node is in the
     * visiting state, as that implies a circular dependency. The exception
     * contains details of the cycle, using elements of the &quot;visiting&quot;
     * stack.
     * <p>
     * If any child has not already been &quot;visited&quot;, this method is
     * called recursively on it.
     * <p>
     * The current target is then added to the ordered list of targets. Note
     * that this is performed after the children have been visited in order to
     * get the correct order. The current target is set to the
     * &quot;visited&quot; state.
     * <p>
     * By the time this method returns, the ordered list contains the sequence
     * of targets up to and including the current target.
     * 
     * @param root
     *            The current target to inspect. Must not be <code>null</code>.
     * @param targetTable
     *            A mapping from names to targets (String to Target). Must not
     *            be <code>null</code>.
     * @param state
     *            A mapping from target names to states (String to String). The
     *            states in question are &quot;VISITING&quot; and
     *            &quot;VISITED&quot;. Must not be <code>null</code>.
     * @param visiting
     *            A stack of targets which are currently being visited. Must not
     *            be <code>null</code>.
     * @param ret
     *            The list to add target names to. This will end up containing
     *            the complete list of dependencies in dependency order. Must
     *            not be <code>null</code>.
     * 
     * @exception BuildException
     *                if a non-existent target is specified or if a circular
     *                dependency is detected.
     */
    private void tsort(String root, Map<String, TargetAdapter> targetTable,
            Map<String, String> state, Stack<String> visiting,
            List<TargetAdapter> ret) throws BuildException {
        state.put(root, VISITING);
        visiting.push(root);

        TargetAdapter target = targetTable.get(root);

        // Make sure we exist
        if (target == null) {
            StringBuffer sb = new StringBuffer("Target \"");
            sb.append(root);
            sb.append("\" does not exist in the project \"");
            sb.append("/*name*/");
            sb.append("\". ");
            visiting.pop();
            if (!visiting.empty()) {
                String parent = visiting.peek();
                sb.append("It is used from target \"");
                sb.append(parent);
                sb.append("\".");
            }
            throw new BuildException(new String(sb));
        }
        for (Enumeration<String> en = target.getDependencies(); en
                .hasMoreElements();) {
            String cur = en.nextElement();
            String m = state.get(cur);
            if (m == null) {
                // Not been visited
                tsort(cur, targetTable, state, visiting, ret);
            } else if (m == VISITING) {
                // Currently visiting this node, so have a cycle
                throw makeCircularException(cur, visiting);
            }
        }
        String p = visiting.pop();
        if (root != p) {
            throw new RuntimeException(
                    "Unexpected internal error: expected to " + "pop " + root
                            + " but got " + p);
        }
        state.put(root, VISITED);
        ret.add(target);
    }

    /**
     * Build an appropriate exception detailing a specified circular dependency.
     * 
     * @param end
     *            The dependency to stop at. Must not be <code>null</code>.
     * @param stk
     *            A stack of dependencies. Must not be <code>null</code>.
     * 
     * @return a BuildException detailing the specified circular dependency.
     */
    private static BuildException makeCircularException(String end,
            Stack<String> stk) {
        StringBuffer sb = new StringBuffer("Circular dependency: ");
        sb.append(end);
        String c;
        do {
            c = stk.pop();
            sb.append(" <- ");
            sb.append(c);
        } while (!c.equals(end));
        return new BuildException(new String(sb));
    }

    public List<TargetAdapter> sortDependencies(String target) {
        return sortDependencies(new String[] { target },
                new Hashtable<String, TargetAdapter>(fTargets), false);
    }

    public List<TargetAdapter> sortDependencies(String[] targets) {
        return sortDependencies(targets, new Hashtable<String, TargetAdapter>(
                fTargets), false);
    }

}
