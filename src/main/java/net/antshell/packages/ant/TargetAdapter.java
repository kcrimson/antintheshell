/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;

import net.antshell.script.ScriptRuntime;
import net.antshell.script.ShellException;

import org.mozilla.javascript.Callable;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.WrappedException;

public class TargetAdapter extends AbstractScriptableObject implements Callable {

    private static final String[] EMPTY_STRING_ARRAY = new String[] {};
    private static final String[] IDS = new String[] { "depends" };
    private final String fName;

    private Callable fCallable;
    private String[] fDependencies;
    private final ScriptRuntime fRuntime;

    public TargetAdapter(ScriptRuntime runtime, String name, Callable callable) {
        fRuntime = runtime;
        fName = name;
        fCallable = callable;
    }

    public TargetAdapter(ScriptRuntime runtime, String name) {
        fRuntime = runtime;
        fName = name;
    }

    @Override
    public Object[] getIds() {
        return IDS;
    }

    @Override
    public boolean has(String name, Scriptable scope) {
        return Arrays.binarySearch(IDS, name) >= 0;
    }

    @Override
    public void put(String name, Scriptable scope, Object value) {
        if ("depends".equals(name)) {
            if (value instanceof String) {
                fDependencies = new String[] { (String) value };
            } else {
                fDependencies = (String[]) Context.jsToJava(value,
                        String[].class);
            }
        }
    }

    @Override
    public Object call(Context cx, Scriptable scope, Scriptable thisObj,
            Object[] args) {

        try {
            fRuntime.executeTargets(new String[] { fName });
        } catch (ShellException e) {
            throw new WrappedException(e);
        }

        return null;
    }

    public Enumeration<String> getDependencies() {
        String[] dependencies = EMPTY_STRING_ARRAY;
        if (fDependencies != null) {
            dependencies = fDependencies;
        }
        return Collections.enumeration(Arrays.asList(dependencies));
    }

    public boolean isCallable() {
        return fCallable != null;
    }

    public void setCallable(Callable callable) {
        fCallable = callable;
    }

    @Override
    public String toString() {
        return fName;
    }

    public Callable getCallable() {
        return fCallable;
    }

}
