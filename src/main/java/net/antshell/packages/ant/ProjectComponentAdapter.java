/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.packages.ant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.tools.ant.ComponentHelper;
import org.apache.tools.ant.IntrospectionHelper;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.IntrospectionHelper.Creator;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

/**
 * Proxies ant tasks/types attributes and nested elements for later execution
 * 
 * @author kcrimson
 * 
 */
public class ProjectComponentAdapter extends AbstractScriptableObject {

    /**
     * Ant component attributes
     */
    private final Map<String, ValueSlot> fAttributes = new HashMap<String, ValueSlot>();

    /**
     * Ant component nested elements
     */
    private final Map<String, ValueSlot> fNestedElements = new HashMap<String, ValueSlot>();

    /**
     * Ids (attributes and nested elements names)
     */
    private Set<String> fIds;

    protected final Project fProject;
    private final Class<?> fComponentClass;

    public ProjectComponentAdapter(Project project, String componentName,
            Scriptable arg) {
        this(project, ComponentHelper.getComponentHelper(project)
                .getComponentClass(componentName), arg);
    }

    public ProjectComponentAdapter(Project project, Class<?> componentClass,
            Scriptable arg) {
        fProject = project;
        fComponentClass = componentClass;

        initilizeIds();

        if (arg != null) {
            initilizeArguments(arg);
        }
    }

    public String getAttribute(String name) {
        ValueSlot valueSlot = fAttributes.get(name);
        return (String) (valueSlot != null ? valueSlot.value : null);
    }

    public Scriptable getNestedElement(String name) {
        ValueSlot valueSlot = fNestedElements.get(name);
        return (Scriptable) (valueSlot != null ? valueSlot.value : null);
    }

    // Scriptable implementation
    @Override
    public Object get(String name, Scriptable scope) {
        ValueSlot attrSlot = fAttributes.get(name);

        if (attrSlot != null && !attrSlot.isEmpty()) {
            return attrSlot.value;
        }

        ValueSlot nestedSlot = fNestedElements.get(name);
        if (nestedSlot != null && !nestedSlot.isEmpty()) {
            return nestedSlot.value;
        }

        return NOT_FOUND;
    }

    @Override
    public Object[] getIds() {
        return fIds.toArray(new String[] {});
    }

    @Override
    public boolean has(String name, Scriptable scope) {
        return fIds.contains(name);
    }

    @Override
    public void put(String name, Scriptable scope, Object value) {

        ValueSlot modifiedSlot = null;
        ValueSlot emptySlot = null;

        if (!(value instanceof Scriptable) && fAttributes.containsKey(name)) {
            modifiedSlot = fAttributes.get(name);
            emptySlot = fNestedElements.get(name);
        } else if (fNestedElements.containsKey(name)) {
            modifiedSlot = fNestedElements.get(name);
            emptySlot = fAttributes.get(name);
        }

        if (modifiedSlot != null) {
            modifiedSlot.value = value;
            if (emptySlot != null && !emptySlot.isEmpty()) {
                emptySlot.value = null;
            }
        }

        // TODO nie wiem co
    }

    // methods

    /**
	 * 
	 */
    public void configure(Object component) {
        IntrospectionHelper helper = getIntrospectionHelper(fComponentClass);

        setAttributes(component, helper);

        setNestedElements(component, helper);

    }

    public Class<?> getComponentClass() {
        return fComponentClass;
    }

    /**
     * Initializes ids based on attributes and nested elements names
     */
    @SuppressWarnings("unchecked")
    private void initilizeIds() {

        IntrospectionHelper helper = getIntrospectionHelper(fComponentClass);

        ArrayList<String> attributesNames = Collections.list(helper
                .getAttributes());
        for (String name : attributesNames) {
            fAttributes.put(name, new ValueSlot());
        }
        // MapUtils.debugPrint(System.out, "label", fAttributes);

        ArrayList<String> nestedElementsNames = Collections.list(helper
                .getNestedElements());
        for (String name : nestedElementsNames) {
            fNestedElements.put(name, new ValueSlot());
        }
        // MapUtils.debugPrint(System.out, "label", fNestedElements);

        HashSet<String> ids = new HashSet<String>();
        ids.addAll(fAttributes.keySet());
        ids.addAll(fNestedElements.keySet());
        fIds = Collections.unmodifiableSet(ids);

    }

    private void initilizeArguments(Scriptable arg) {

        IntrospectionHelper helper = getIntrospectionHelper(fComponentClass);

        String idName;
        for (Object id : arg.getIds()) {
            idName = (String) id;
            ValueSlot attrSlot = fAttributes.get(idName);

            if (attrSlot != null) {
                // TODO check if value is string if not try to convert it to
                // string
                Object obj = ScriptableObject.getProperty(arg, idName);

                String value;
                if (NativeJavaObject.canConvert(obj, String.class)) {
                    value = (String) Context.jsToJava(obj, String.class);
                } else {
                    // throw new Rhino();
                    throw new RuntimeException(
                            "co to jest za kurwa co to do string'a go nie mozna wbic");
                }

                // TODO throw exception unsupported type
                attrSlot.value = value;
                continue;
            }

            ValueSlot nestedSlot = fNestedElements.get(idName);
            if (nestedSlot != null) {

                Object value = ScriptableObject.getProperty(arg, idName);
                if (value instanceof ProjectComponentAdapter) {
                    nestedSlot.value = value;
                } else if (value instanceof Scriptable) {
                    Class<?> nestedComponentClass = (Class<?>) helper
                            .getNestedElementMap().get(idName);
                    nestedSlot.value = new ProjectComponentAdapter(fProject,
                            nestedComponentClass, (Scriptable) value);
                } else {
                    // TODO throw exception unsupported type
                }
            }
        }
    }

    private IntrospectionHelper getIntrospectionHelper(Class<?> componentClass) {
        return IntrospectionHelper.getHelper(fProject, componentClass);
    }

    private void setNestedElements(Object component, IntrospectionHelper helper) {
        Enumeration<String> nestednames = helper.getNestedElements();
        String nestedname;
        ValueSlot nestedslot;
        while (nestednames.hasMoreElements()) {
            nestedname = nestednames.nextElement();
            nestedslot = fNestedElements.get(nestedname);
            if (nestedslot != null && !nestedslot.isEmpty()) {

                ProjectComponentAdapter adapter;
                if (nestedslot.value instanceof ProjectComponentAdapter) {

                    adapter = (ProjectComponentAdapter) nestedslot.value;

                } else if (nestedslot.value instanceof Scriptable) {
                    Class<?> componentClass = (Class<?>) helper
                            .getNestedElementMap().get(nestedname);
                    adapter = new ProjectComponentAdapter(fProject,
                            componentClass, (Scriptable) nestedslot.value);
                } else {
                    throw new UnsupportedOperationException();
                }

                Creator elementCreator = helper.getElementCreator(fProject, "",
                        component, nestedname, null);
                Object nestedobj = elementCreator.create();
                adapter.configure(nestedobj);
                elementCreator.store();
            }
        }
    }

    private void setAttributes(Object component, IntrospectionHelper helper) {
        Enumeration<String> attrnames = helper.getAttributes();
        String attrname;
        ValueSlot attrslot;
        while (attrnames.hasMoreElements()) {
            attrname = attrnames.nextElement();
            attrslot = fAttributes.get(attrname);
            if (attrslot != null && !attrslot.isEmpty()) {
                helper.setAttribute(fProject, component, attrname,
                        (String) attrslot.value);
            }
        }
    }

    private class ValueSlot {

        private Object value;

        public boolean isEmpty() {
            return value == null;
        }

    }

}
