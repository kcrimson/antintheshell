/*
 *    Copyright 2009 AntInTheShell team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.antshell.launcher;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.Parser;
import org.codehaus.classworlds.ClassRealm;
import org.codehaus.classworlds.ClassWorld;
import org.codehaus.classworlds.NoSuchRealmException;

public class Main {

    /**
     * 
     */
    private static final String PRINTHELP_OPTION = "h";
    private static final String BUILDFILE_OPTION = "f";
    private static final String DEFAULT_BUILD_FILE = "build.js";

    /**
     * @param args
     */
    public static void main(String[] args, ClassWorld classworld) {
        Parser parser = new BasicParser();

        try {
            Options options = getOptions();

            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption(PRINTHELP_OPTION)) {
                HelpFormatter formatter = new HelpFormatter();

                formatter.printHelp("antshell", options);
                System.exit(0);
            }

            String shellargs = rejoinCommandArgs(cmd.getArgs());

            String buildfile = cmd.getOptionValue(BUILDFILE_OPTION,
                    DEFAULT_BUILD_FILE);
            File file = new File(buildfile);

            if (!file.exists()) {
                System.out.println("Build file " + file.getCanonicalPath()
                        + " doesn't exist.");
                return;
            }

            System.out.println("Build file " + file.getCanonicalPath());

            Launcher launcher = instantiateLauncher(classworld);

            launcher.launch(file, shellargs);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchRealmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // array of arguments as command line
    private static String rejoinCommandArgs(String[] args) {
        StringBuffer buffer = new StringBuffer();
        for (String arg : args) {
            buffer.append(arg);
            buffer.append(' ');
        }

        return buffer.toString().trim();
    }

    private static Options getOptions() {
        Options options = new Options();

        Option opt = OptionBuilder.hasArg().withArgName("file").withLongOpt(
                "buildfile").withDescription("use give build file").create(
                BUILDFILE_OPTION);

        options.addOption(opt);

        opt = OptionBuilder.withLongOpt("help").withDescription(
                "print this message").create(PRINTHELP_OPTION);
        options.addOption(opt);

        return options;
    }

    private static Launcher instantiateLauncher(ClassWorld classworld)
            throws NoSuchRealmException, ClassNotFoundException,
            InstantiationException, IllegalAccessException {
        ClassRealm realm = classworld.getRealm("shell");
        ClassLoader classl = realm.getClassLoader();
        Class<?> launchercl = classl
                .loadClass("net.antshell.script.DefaultLauncher");
        Launcher launcher = (Launcher) launchercl.newInstance();
        return launcher;
    }

}
